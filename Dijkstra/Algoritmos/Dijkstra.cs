﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorithmsVisualizer.Structures;

namespace AlgorithmsVisualizer.Algorithms
{
    public class Dijkstra<T> where T : Nodo
    {
        protected Grafo<T> mGrafo;
        //private IDijkstraListener mListener;

        #region Constructor
        public Dijkstra(Grafo<T> pGrafo)
        {
            mGrafo = pGrafo;
        }
        
        #endregion

        public async Task<Tuple<Int32, List<Nodo>>> Resolver(Nodo nodoOrigen, Nodo nodoDestino)
        {
            int origen = nodoOrigen.Indice, destino = nodoDestino.Indice;
            // almacena la ruta paso-a-paso para ir del origen al destino
            List<int> Camino = new List<int>();
            // como llego a un determinado nodo? - default: -1
            List<int> ComoLlegoA = Enumerable.Repeat(-1, mGrafo.GetSize()).ToList();
            // a que distancia del punto de origen se encuentra cada nodo? - default: infinito
            List<int> Distancias = Enumerable.Repeat(Int32.MaxValue, mGrafo.GetSize()).ToList();
            // cuales son las rutas que estan pendientes por procesar? (ordenadas ascendentemente por distancia)
            MinHeap Pendientes = new MinHeap();


            // existe una distancia 0 del origen a si mismo
            Distancias[origen] = 0;
            //se agregan las aristas del nodo de origen a la cola de pendientes
            Pendientes.Add(new Arista() { Destino = mGrafo.Nodos[origen], Peso = Distancias[origen]});

            //mientras haya elementos pendientes
            while (Pendientes.Count > 0)
            {
                //la arista que estoy evaluando es la que tiene menor distancia dentro de la cola
                Arista AristaQueEstoyEvaluando = Pendientes.RemoveMin();
                Nodo NodoQueEstoyEvaluando = AristaQueEstoyEvaluando.Destino;
                int IndiceEvaluando = AristaQueEstoyEvaluando.IndiceDestino;
                await NodoEnEstado(NodoQueEstoyEvaluando, Estados.EVALUANDO);

                if (IndiceEvaluando == destino) //Hallamos la solucion!
                    break;

                if (Distancias[IndiceEvaluando] >= AristaQueEstoyEvaluando.Peso)
                {
                    foreach(Arista arista in NodoQueEstoyEvaluando.Aristas)
                    {
                        await EvaluandoArista(NodoQueEstoyEvaluando, arista);
                        //La distancia para llegar al del cual parte la arista, mas el peso de la arista
                        int tempDistancia = Distancias[IndiceEvaluando] + arista.Peso;
                        if (tempDistancia < Distancias[arista.IndiceDestino])
                        {
                            Distancias[arista.IndiceDestino] = tempDistancia;
                            await SeteandoDistancia(arista.IndiceDestino, tempDistancia);

                            ComoLlegoA[arista.IndiceDestino] = IndiceEvaluando;
                            await SeteandoComoLlego(arista.Destino, NodoQueEstoyEvaluando);

                            Pendientes.Add( new Arista() { Destino = arista.Destino ,Peso = tempDistancia });
                        }
                    }
                }
                await NodoEnEstado(NodoQueEstoyEvaluando, Estados.PROCESADO);
            }

            List<Nodo> ruta = new List<Nodo>();
            ruta.Add(nodoDestino);
            await AgregandoARuta(nodoDestino);
            int temp = ComoLlegoA[destino];
            while (temp!=-1)
            {
                ruta.Insert(0, mGrafo.Nodos[temp]);
                await AgregandoARuta(mGrafo.Nodos[temp]);
                temp = ComoLlegoA[temp];
            }

            return new Tuple<Int32, List<Nodo>>(Distancias[destino], ruta);
        }

        public virtual async Task EvaluandoArista(Nodo origen, Arista arista) { }
        public virtual async Task SeteandoComoLlego(Nodo destino, Nodo comoLLego) { }
        public virtual async Task SeteandoDistancia(int nodo,int peso) { }
        public virtual async Task NodoEnEstado(Nodo nodo, Estados estado) { }
        public virtual async Task AgregandoARuta(Nodo nodo) { }

    }

    //public interface IDijkstraListener{
    //    void EvaluandoArista(Arista arista);
    //    void SeteandoComoLlego(int destino, int comoLLego);
    //}

}
