﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorithmsVisualizer.Structures;

namespace AlgorithmsVisualizer.Algorithms
{
    public class DFS<T> where T : Nodo
    {
        protected Grafo<T> mGrafo;

        #region Constructor
        public DFS(Grafo<T> pGrafo)
        {
            mGrafo = pGrafo;
        }
        #endregion

        public Task<List<int>> Resolver(Nodo origen, Nodo destino)
        {
            return Resolver(origen.Indice, destino.Indice);
        }
        public async Task<List<int>> Resolver(int origen, int destino)
        {
            Stack<int> rutas = new Stack<int>();
            List<bool> visitados = Enumerable.Repeat(false, mGrafo.GetSize()).ToList();

            bool resultado = await _Resolver(rutas, visitados, origen, destino);

            List<int> rutaList = new List<int>();
            return rutaList;
        }

        private async Task<bool> _Resolver(Stack<int> ruta, List<bool> visitados, int evaluando, int objetivo)
        {
            ruta.Push(evaluando);
            await AgregandoStack(evaluando);
            Nodo nodo = mGrafo.Nodos[evaluando];
            await MarcarComoVisitado(nodo);
            if (evaluando == objetivo)
            {
                await EncontreObjetivo(nodo);
                return true;
            }
            visitados[evaluando] = true;
            foreach (Arista arista in nodo.Aristas)
            {
                int pos = arista.IndiceDestino; // g->nodos[evaluando]->arcos[i];
                if (!visitados[pos]) // si ese nodo no ha sido visitado
                {
                    await EvaluandoArista(nodo, arista);
                    if (await _Resolver(ruta, visitados, pos, objetivo))
                        return true;
                }
            }
            visitados[evaluando] = false;
            await SacandoStack(evaluando);
            ruta.Pop();
            return false;   
        }

        #region Metodos que deben ser sobreescritos

        public virtual async Task EvaluandoArista(Nodo origen, Arista arista) { }
        public virtual async Task MarcarComoVisitado(Nodo nodo) { }
        public virtual async Task AgregandoStack(int nodo) { }
        public virtual async Task SacandoStack(int nodo) { }
        public virtual async Task EncontreObjetivo(Nodo nodo) { }

        #endregion

    }
}
