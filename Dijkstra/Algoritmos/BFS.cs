﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorithmsVisualizer.Structures;

namespace AlgorithmsVisualizer.Algorithms
{

    public class BFS<T> where T : Nodo
    {
        protected Grafo<T> mGrafo;

        #region Constructor
        public BFS(Grafo<T> pGrafo)
        {
            mGrafo = pGrafo;
        }
        #endregion

        public async Task<List<Nodo>> Resolver(Nodo origen, Nodo destino)
        {
            try
            {
                Queue<List<Nodo>> rutas = new Queue<List<Nodo>>();
                List<int> estados = Enumerable.Repeat(0, mGrafo.GetSize()).ToList();
                List<int> distancias = new List<int>();

                List<Nodo> ruta = new List<Nodo>();
                ruta.Add(origen);
                rutas.Enqueue(ruta);
                await Encolando(ruta);

                while (rutas.Count > 0)
                {
                    List<Nodo> rutaEvaluando = rutas.Dequeue();
                    Nodo evaluando = rutaEvaluando.Last();
                    await Desencolando();
                    estados[evaluando.Indice] = 1; // ha sido visitado
                    await NodoEnEstado(evaluando, Estados.EVALUANDO);

                    foreach (Arista arista in evaluando.Aristas)
                    {
                        await EvaluandoArista(evaluando, arista);

                        if (arista.IndiceDestino == destino.Indice)
                        {
                            await EncontreObjetivo(rutaEvaluando);
                            return rutaEvaluando;
                        }

                        if (estados[arista.IndiceDestino] == 0)
                        {
                            List<Nodo> temp = new List<Nodo>(rutaEvaluando);
                            temp.Add(arista.Destino);
                            rutas.Enqueue(temp);
                            await Encolando(temp);
                        }
                    }
                    estados[evaluando.Indice] = 2; // ha sido procesado
                    await NodoEnEstado(evaluando, Estados.PROCESADO);

                }

                return null;
            }catch(Exception e)
            {
                int a;
                return null;
            }
            
        }
        
        #region Metodos que deben ser sobreescritos

        public virtual async Task EvaluandoArista(Nodo origen, Arista arista) { }
        public virtual async Task NodoEnEstado(Nodo nodo, Estados estado) { }
        public virtual async Task Encolando(List<Nodo> ruta) { }
        public virtual async Task Desencolando() { }
        public virtual async Task EncontreObjetivo(List<Nodo> ruta) { }

        #endregion

    }
}
