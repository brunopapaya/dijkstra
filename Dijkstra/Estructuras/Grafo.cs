﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsVisualizer.Structures
{
    public class Grafo<TNodo> where TNodo : Nodo
    {

        private static int MAX_SIZE = 100;
        private int size,currentSize = 0;
        private List<TNodo> nodos;
        public bool Dirigido { get; set; }
        public List<TNodo> Nodos
        {
            get
            {
                return nodos;
            }
        }

        public Grafo() : this(MAX_SIZE){}

        public Grafo(int pSize)
        {
            if (pSize > MAX_SIZE)
                throw new ArgumentOutOfRangeException("pSize", "El grafo no puede tener más de " + MAX_SIZE + " nodos");
            size = pSize;
            Dirigido = true;
            nodos = new List<TNodo>(pSize);
        }
        
        protected void AgregarNodo(TNodo nodo)
        {
            nodos.Add(nodo);
            nodo.Indice = currentSize++;
        }

        protected virtual void AgregarArista(Nodo origen, Nodo destino,int peso)
        {
            origen.AgregarArista(destino, peso, Dirigido);
            if (!Dirigido)
                destino.AgregarArista(origen, peso, Dirigido);
        }

        public int GetSize()
        {
            return currentSize;
        }
        

        public void Limpiar()
        {
            if (nodos != null && nodos.Count > 0)
                nodos.Clear();
            nodos = new List<TNodo>(size);
        }

    }
}
