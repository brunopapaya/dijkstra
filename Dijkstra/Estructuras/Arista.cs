﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsVisualizer.Structures
{
    public class Arista
    {
        public int Peso { get; set; }
        public Nodo Destino { get; set; }
        public int IndiceDestino
        {
            get
            {
                return Destino != null ? Destino.Indice : -1;
            }
        }
    }
}
