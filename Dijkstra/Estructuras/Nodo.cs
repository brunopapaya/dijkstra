﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmsVisualizer.Structures
{
    public class Nodo
    {
        public int Indice{ get; set; }
        public List<Arista> Aristas { get; set; } 

        public Nodo()
        {
            Indice = -1;
            Aristas = new List<Arista>();
        }

        public override string ToString()
        {
            return "" + (Indice+1);
        }

        public virtual void AgregarArista(Nodo destino, int peso, bool esDirigida)
        {
            Aristas.Add(new Arista() { Destino = destino, Peso = peso });
        }

    }

}
