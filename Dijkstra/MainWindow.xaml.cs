﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AlgorithmsVisualizer.UI;
using System.Windows.Media.Animation;
using Dijkstra;

namespace AlgorithmsVisualizer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window,IGrafoListener
    {
        GrafoUI mGrafoUI;
        NodoUI origen, destino;
        int paso = 0;

        public MainWindow()
        {
            InitializeComponent();
            lblResultado.Visibility = Visibility.Hidden;
        }
        

        private void ContinueMouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (paso)
            {
                case 0:
                    {
                        ControlsArea.Visibility = Visibility.Hidden;
                        paso = 1;
                        mGrafoUI = new GrafoUI(GraphCanvas);
                        mGrafoUI.Dirigido = EsDirigido.IsChecked.Value;
                        mGrafoUI.PesosAutomaticos = PesosAutomáticos.IsChecked.Value;
                        mGrafoUI.Dialog = PesoDialog;
                        Hint.Text = "1. Arma tu grafo!";
                        Storyboard sb = this.FindResource("HintFade2") as Storyboard;
                        sb.Begin();
                        break;
                    }
                case 1:
                    if (mGrafoUI.GetSize() < 6)
                    {
                        MessageBox.Show("¡El grafo debe tener por lo menos 6 nodos!", "Advertencia");
                    }
                    else
                    {
                        paso = 2;
                        mGrafoUI.Mode = GrafoModo.SELECCION;
                        mGrafoUI.GrafoUIListener = this;
                        mGrafoUI.Limpiar();
                        Hint.Text = "2. Selecciona el nodo de origen";
                        Storyboard sb = this.FindResource("HintFade2") as Storyboard;
                        sb.Begin();

                    }
                    break;
                case 2:
                    if (origen == null)
                        MessageBox.Show("Escoge el nodo de origen");
                    else
                    {
                        paso = 3;
                        origen.Locked = true;
                        Hint.Text = "3. Selecciona el nodo destino";
                        Storyboard sb = this.FindResource("HintFade2") as Storyboard;
                        sb.Begin();
                    }
                    break;
                case 3:
                    if (destino == null)
                        MessageBox.Show("Escoge el nodo destino", "Advertencia");
                    else
                    {
                        ComenzarDijkstra();
                    }
                    break;
            }

        }

        private async void ComenzarDijkstra()
        {
            //Setear cuadro de monitoreo
            mGrafoUI.Limpiar();
            //Setear algoritmo de dijkstra
            DijkstraUI mDijkstraUI = new DijkstraUI(mGrafoUI);
            mDijkstraUI.InicializarTabla(DijkstraGrid);
            Ruta.Text = "";
            //mDijkstraUI.Limpiar();
            var resultado = await mDijkstraUI.Resolver(origen, destino);
            var ruta = resultado.Item2;
            var distanciaTotal = resultado.Item1;
            if (ruta != null && ruta.Count > 1)
            {
                //Hay solucion
                string temp = "";
                int c = 0;
                foreach (var nodo in ruta)
                {
                    if (c++ == 0)
                        temp = temp + nodo.ToString();
                    else
                        temp = temp + " -> " + nodo.ToString();
                }
                Ruta.Text = temp;
            }
            else
                Ruta.Text = "No hay camino!";

            lblResultado.Content = "Distancia desde el nodo #"
                                        + origen.ToString() + " al nodo #"
                                        + destino.ToString() + ": "
                                        + (distanciaTotal == Int32.MaxValue ? "Inalcanzable" : distanciaTotal.ToString() + ", con " + (ruta.Count - 2) + " nodo(s) intermedio(s)");

            lblResultado.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DFSWindow win = new DFSWindow();
            win.Show();
        }

        private void ClearMouseDown(object sender, MouseButtonEventArgs e)
        {
            paso = 0;
            
            ControlsArea.Visibility = Visibility.Visible;
            Storyboard sb = this.FindResource("HintFade2") as Storyboard;
            sb.Begin();
            Hint.Text = "Configuración Inicial";
            GraphCanvas.Children.Clear();
            mGrafoUI = null;
            lblResultado.Visibility = Visibility.Hidden;
        }

        private void EsDirigido_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void PesosAutomáticos_Checked(object sender, RoutedEventArgs e)
        {

        }

        public void NodoWasSelected(NodoUI nodo)
        {
            switch (paso)
            {
                case 2:
                    if (origen != null)
                        origen.Selected = false;
                    origen = nodo;
                    origen.Selected = true;
                    break;
                case 3:
                    if (nodo.Locked)
                    {
                        MessageBox.Show("El nodo de origen no puede ser igual al nodo destino","Advertencia");
                    }
                    else
                    {
                        if (destino != null)
                            destino.Selected = false;
                        destino = nodo;
                        destino.Selected = true;

                    }
                    break;
            }
        }
        /*private void PesoArista_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    int n = Int32.Parse(TextPeso.Text);
                    if (n >= 0)//No permite negativos
                    {

                    }
                    else
                        MessageBox.Show("Valores ingresados incorrectos", "Error");
                }
            }
            catch (FormatException fe)
            {
                MessageBox.Show("Valores ingresados incorrectos", "Error");
            }
        }
        */
    }
}
