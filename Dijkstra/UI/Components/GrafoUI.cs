﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using AlgorithmsVisualizer.Structures;
using Dijkstra.UI.Components;
using System.Windows.Input;

namespace AlgorithmsVisualizer.UI
{
    public enum GrafoModo { CREACION,SELECCION}

    public class GrafoUI : Grafo<NodoUI>,INodoUIListener
    {
        private Canvas mCanvas;
        private double NodosSize = 30;
        private NodoUI NodoSeleccionado = null;
        private bool nodoClicked = false;
        public GrafoModo Mode { get; set; }
        public IGrafoListener GrafoUIListener { get; set; }
        public bool PesosAutomaticos { get; set; }
        public DialogPeso Dialog { get; set; }

        private KeyEventHandler ev;

        private GrafoUI() { }
        public GrafoUI(Canvas pCanvas)
        {
            if (pCanvas == null)
                throw new ArgumentNullException("No puedo trabajar con un canvas nulo!!");
            mCanvas = pCanvas;
            Mode = GrafoModo.CREACION;
            mCanvas.MouseDown += CanvasMouseDown;
            PesosAutomaticos = true;
        }

        private void CanvasMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            switch (Mode)
            {
                case GrafoModo.CREACION:
                    if (nodoClicked)
                    {
                        nodoClicked = false;
                        return;
                    }
                    var coord = e.GetPosition(mCanvas);
                    var nuevo = new NodoUI(mCanvas, coord.X, coord.Y, NodosSize, this);
                    AgregarNodo(nuevo);
                    nuevo.UpdateLabel();
                    break;
                case GrafoModo.SELECCION:
                    break;
            }
        }

        protected override void AgregarArista(Nodo origen, Nodo destino, int peso)
        {
            base.AgregarArista(origen, destino, peso);
        }

        public void SetNodosSize(double newSize)
        {
            NodosSize = newSize;
        }

        public async void NodoClicked(NodoUI nodo, Point coord)
        {
            switch (Mode)
            {
                case GrafoModo.CREACION:
                    nodoClicked = true;
                    if (NodoSeleccionado == null)
                    {
                        NodoSeleccionado = nodo;
                    }
                    else
                    {
                        if (NodoSeleccionado.Indice != nodo.Indice) // unir nodos
                        {
                            if (PesosAutomaticos)
                            {
                                AgregarArista(NodoSeleccionado, nodo, CalcularDistancia(NodoSeleccionado, nodo));
                            }
                            else
                            {
                                if (Dialog != null) // Si se ha seteado el dialog
                                {
                                    ev = (o, e) =>
                                    {
                                        try
                                        {
                                            if (e.Key == Key.Enter)
                                            {
                                                TextBox tb = (TextBox)o;
                                                int n = Int32.Parse(tb.Text);
                                                AgregarArista(NodoSeleccionado, nodo, n);
                                                Dialog.Visibility = Visibility.Collapsed;
                                                nodo.Selected = false;
                                                NodoSeleccionado.Selected = false;
                                                NodoSeleccionado = null;
                                                Dialog.RemoveListener(ev);
                                            }
                                            else if (e.Key == Key.Escape)
                                            {
                                                Dialog.Visibility = Visibility.Collapsed;
                                                nodo.Selected = false;
                                                NodoSeleccionado.Selected = false;
                                                NodoSeleccionado = null;
                                            }
                                        }
                                        catch (FormatException fe)
                                        {
                                            MessageBox.Show("Valores ingresados incorrectos", "Error");
                                        }
                                    };
                                    Dialog.SetListener(ev);
                                    Dialog.SetVisible();
                                    return;
                                }
                            }


                            nodo.Selected = false;
                        }
                        NodoSeleccionado.Selected = false;
                        NodoSeleccionado = null;
                    }
                    break;
                case GrafoModo.SELECCION:
                    if (GrafoUIListener != null)
                        GrafoUIListener.NodoWasSelected(nodo);
                    break;
            }
        }

        

        public void Limpiar()
        {
            foreach(var nodo in Nodos)
            {
                nodo.SetState(Algorithms.Estados.NO_VISITADO);
            }
        }


        private int CalcularDistancia(NodoUI nodo1, NodoUI nodo2)
        {
            return (int) Math.Sqrt(Math.Pow(nodo1.X-nodo2.X,2)+ Math.Pow(nodo1.Y - nodo2.Y, 2));
        }
    }

    public interface IGrafoListener
    {
        void NodoWasSelected(NodoUI nodo);
    }
}
