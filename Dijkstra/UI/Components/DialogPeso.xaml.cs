﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dijkstra.UI.Components
{
    /// <summary>
    /// Interaction logic for DialogPeso.xaml
    /// </summary>
    public partial class DialogPeso : UserControl
    {
        public DialogPeso()
        {
            InitializeComponent();
        }

        public void SetVisible()
        {
            Visibility = Visibility.Visible;
            TextPeso.Text = "";
            TextPeso.Focus();
        }

        public void SetListener(KeyEventHandler evento)
        {
            TextPeso.PreviewKeyDown += evento;
        }

        public void RemoveListener(KeyEventHandler evento)
        {
            TextPeso.PreviewKeyDown -= evento;
        }

    }
}
