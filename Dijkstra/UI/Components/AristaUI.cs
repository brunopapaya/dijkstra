﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using AlgorithmsVisualizer.Structures;
using Petzold.Media2D;

namespace AlgorithmsVisualizer.UI
{
    public class AristaUI : Arista
    {
        private Canvas mCanvas;
        private NodoUI mOrigen, mDestino;
        private int mPeso;
        private Shape mShape;
        private TextBlock tbPeso;

        private AristaUI() { }

        public AristaUI(Canvas pCanvas, NodoUI pOrigen, NodoUI pDestino, int pPeso, bool esDirigida)
        {
            if (pOrigen == null)
                throw new ArgumentNullException("pOrigen", "El nodo de origen no puede ser nulo");
            if (pDestino == null)
                throw new ArgumentNullException("pDestino", "El nodo destino no puede ser nulo");

            Destino = pDestino;

            mOrigen = pOrigen;
            mDestino = pDestino;
            mPeso = pPeso;
            mCanvas = pCanvas;

            // calcular angulo de dirección
            double yMedio = (mDestino.Y + mOrigen.Y) / 2, xMedio = (mDestino.X + mOrigen.X) / 2;
            double deltaX = mDestino.X - mOrigen.X, deltaY = mDestino.Y - mOrigen.Y;
            double finalSize = Math.Sqrt(Math.Pow(mDestino.X - mOrigen.X, 2) + Math.Pow(mDestino.Y - mOrigen.Y, 2)) - mOrigen.Size;
            var angleInRadians = Math.Atan2(deltaY, deltaX);
            var angleInDegrees = angleInRadians * 180 / Math.PI;

            // definir el objeto para el peso, pero su ubicación
            // dependerá de si la arista es dirigida (se colocará cerca al nodo origen)
            // o si es no dirigida (se colocará en el centro de la arista)
            tbPeso = new TextBlock();
            tbPeso.Text = mPeso.ToString();
            tbPeso.Background = Brushes.White;
            tbPeso.FontSize = 10;
            tbPeso.MaxHeight = 666; // propiedad para identificar los TextBlocks de otros elementos de UI

            if (esDirigida)
            {
                // texto se coloca cerca al nodo origen
                double xPeso = mOrigen.X + Math.Cos(angleInRadians) * mOrigen.Size - 6*tbPeso.Text.ToString().Length / 2;
                double yPeso = mOrigen.Y + Math.Sin(angleInRadians) * mOrigen.Size - 10/2;
                Canvas.SetLeft(tbPeso, xPeso);
                Canvas.SetTop(tbPeso, yPeso);
                tbPeso.Background = Brushes.Transparent;
                mCanvas.Children.Insert(0, tbPeso);

                // construcción de arista dirigida
                ArrowLine arrowLine = new ArrowLine();
                arrowLine.X1 = xMedio - finalSize / 2;
                arrowLine.Y1 = yMedio;
                arrowLine.X2 = xMedio + finalSize / 2;
                arrowLine.Y2 = yMedio;
                arrowLine.Stroke = Brushes.LightGray;
                arrowLine.StrokeThickness = 2;
                RotateTransform rotateTransform = new RotateTransform(angleInDegrees, xMedio, yMedio);
                arrowLine.RenderTransform = rotateTransform;
                mCanvas.Children.Insert(0, arrowLine);
                mShape = arrowLine;

            }
            else
            {
                // texto se coloca en el centro de la arista
                double xPeso = xMedio;
                double yPeso = yMedio;
                Canvas.SetLeft(tbPeso, xPeso);
                Canvas.SetTop(tbPeso, yPeso);
                tbPeso.Background = Brushes.Transparent;
                mCanvas.Children.Insert(0, tbPeso);

                // construcción de arista no dirigida
                Line simpleLine = new Line();
                simpleLine.X1 = mOrigen.X;
                simpleLine.Y1 = mOrigen.Y;
                simpleLine.X2 = mDestino.X;
                simpleLine.Y2 = mDestino.Y;
                simpleLine.Stroke = Brushes.LightGray;
                mCanvas.Children.Insert(0, simpleLine);
                mShape = simpleLine;
            }
        }

        public void Highlight(bool highlight)
        {
            if (highlight)
            {
                mShape.Stroke = Brushes.Red;
                mShape.StrokeThickness = 4;
            }
            else
            {
                mShape.Stroke = Brushes.LightGray;
                mShape.StrokeThickness = 2;
            }
        }

    }

}
