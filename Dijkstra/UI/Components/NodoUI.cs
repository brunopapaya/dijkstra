﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Windows.Media;
using System.Windows;
using AlgorithmsVisualizer.Structures;
using AlgorithmsVisualizer.Algorithms;
using System.Globalization;

namespace AlgorithmsVisualizer.UI
{
    public class NodoUI : Nodo
    {

        private static SolidColorBrush STROKE_COLOR = (SolidColorBrush)Application.Current.Resources["MainDarkColorBrush"];

        private static SolidColorBrush NORMAL_COLOR = (SolidColorBrush)Application.Current.Resources["NormalNodeColorBrush"];
        private static SolidColorBrush SELECTED_COLOR = (SolidColorBrush)Application.Current.Resources["SelectedNodeColorBrush"];
        
        private static SolidColorBrush PENDING_EVALUATION_COLOR = (SolidColorBrush)Application.Current.Resources["PendingEvaluationNodeColorBrush"];
        private static SolidColorBrush EVALUATING_COLOR = (SolidColorBrush)Application.Current.Resources["EvaluatingNodeColorBrush"];
        private static SolidColorBrush PROCESSED_COLOR = (SolidColorBrush)Application.Current.Resources["ProcessedNodeColorbrush"];
        private static SolidColorBrush SOLUTION_COLOR = (SolidColorBrush)Application.Current.Resources["SolutionNodeColorBrush"];

        private Canvas mCanvas;
        private TextBlock mText;
        private INodoUIListener mListener;
        private static double MAX_SIZE = 300;
        private double _x, _y;
        public double X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
                Canvas.SetLeft(mShape, _x - mSize / 2);
            }

        }
        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
                Canvas.SetTop(mShape, _y - mSize / 2);
            }
        }
        private Ellipse mShape;
        private double mSize = 80;
        public double Size
        {
            get
            {
                return mSize;
            }
            set
            {
                if (value < MAX_SIZE) {
                    mSize = value;
                    mShape.Width = mShape.Height = Size;
                }
                else
                {
                    throw new ArgumentOutOfRangeException("Un nodo no puede tener un tamaño mayor a " + MAX_SIZE);
                }

            }
        }
        private bool _selected;
        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                if (!Locked)
                {
                    _selected = value;
                    if (value)
                    {
                        //SetStroke(2, Brushes.LimeGreen);
                        SetColor(Brushes.LimeGreen);
                    }
                    else
                    {
                        SetColor(Brushes.LightGray);
                        mShape.StrokeThickness = 0;
                    }
                }
            }
        }
        public List<AristaUI> AristasVisuales { get; set; }
        public bool Locked { get; set; }

        private NodoUI() { }
        public NodoUI(Canvas canvas,double pX, double pY, double pSize, INodoUIListener listener) : base()
        {
            Locked = false;
            AristasVisuales = new List<AristaUI>();
            mShape = new Ellipse();
            Size = pSize;
            mShape.Stroke = Brushes.DarkGreen;

            //Setear coordenadas
            X = pX;
            Y = pY;

            Selected = false;
            mCanvas = canvas;
            canvas.Children.Add(mShape);
            mShape.MouseDown += ShapeClicked;
            mListener = listener;

        }

        //public override string ToString()
        //{
        //    char c = 'A';
        //    c+= (char) Indice;
        //    return "" + c;
        //}


        public void UpdateLabel()
        {
            if (mText != null)
                mCanvas.Children.Remove(mText);
            //Para dibujar el nombre del nodo
            mText = new TextBlock();
            mText.Foreground = Brushes.White;
            mText.Text = ToString();
            double labelHeight = (MeasureString(ToString())).Height;
            Canvas.SetTop(mText, _y - labelHeight / 2);

            double labelWidth = (MeasureString(ToString())).Width;
            Canvas.SetLeft(mText, _x - labelWidth / 2);

            mText.MouseDown -= ShapeClicked;
            mText.MouseDown += ShapeClicked;

            mCanvas.Children.Add(mText);
        }

        private Size MeasureString(string candidate)
        {
            var formattedText = new FormattedText(
                candidate,
                CultureInfo.CurrentUICulture,
                FlowDirection.LeftToRight,
                new Typeface(this.mText.FontFamily, this.mText.FontStyle, this.mText.FontWeight, this.mText.FontStretch),
                this.mText.FontSize,
                Brushes.Black);

            return new Size(formattedText.Width, formattedText.Height);
        }

        private void ShapeClicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Selected = !Selected;
            if (mListener != null)
                mListener.NodoClicked(this, e.GetPosition(mCanvas));
        }

        public void SetColor(Brush newColor)
        {
            mShape.Fill = newColor;
        }

        private Estados mEstado;
        public void SetState(Estados estado)
        {
            mEstado = estado;
            SolidColorBrush newColor;
            switch (estado)
            {
                case Estados.POR_EVALUAR:
                    newColor = PENDING_EVALUATION_COLOR;
                    break;
                case Estados.EVALUANDO:
                    newColor = EVALUATING_COLOR;
                    break;
                case Estados.PROCESADO:
                    newColor = PROCESSED_COLOR;
                    break;
                case Estados.SOLUCION:
                    newColor = SOLUTION_COLOR;
                    break;
                default:
                    newColor = NORMAL_COLOR;
                    break;
            }
            mShape.Fill = newColor;
        }

        public Estados getState()
        {
            return mEstado;
        }

        public void SetStroke(int strokeWidth,Brush strokeColor)
        {
            mShape.Stroke = strokeColor;
            mShape.StrokeThickness = strokeWidth;
        }

        public void SetOpacity(double opacity)
        {
            if (opacity < 0 || opacity > 1)
                throw new ArgumentOutOfRangeException("La opacidad debe estar entre 0 y 1, siendo 0 totalmente transparente y 1 totalmente opaco");
            mShape.Opacity = opacity;
        }

        public override void AgregarArista(Nodo destino, int peso, bool esDirigida)
        {
            base.AgregarArista(destino, peso,esDirigida);
            NodoUI destinoAsUI = destino as NodoUI;
            AristasVisuales.Add(new AristaUI(mCanvas, this, destinoAsUI, peso, esDirigida));

            // procedimiento para pasar todos los TextBlocks al frente del canvas
            // si no se hace esto algunas aristas se sobrepondrán sobre los pesos
            // haciendo que los pesos no se vean

            // lista auxiliar para guardar todos los TextBlock que están en el canvas
            List<TextBlock> auxList = new List<TextBlock>();

            // guardar los TextBlock que están en el canvas
            foreach(UIElement current in mCanvas.Children) 
            {
                var converted = current as TextBlock;
                if(converted != null)
                {
                    if(converted.MaxHeight == 666)
                    {
                        auxList.Add(converted);
                    }
                }
            }

            // sacar los TextBlock del canvas y volverlos a agregar al mismo
            // la diferencia es que ahora estarán por encima de los demás elementos UI
            // asegurando que los pesos siempre sean visibles
            foreach(TextBlock currentTb in auxList)
            {
                mCanvas.Children.Remove(currentTb);
                mCanvas.Children.Add(currentTb);
            }
        }

    }

    public interface INodoUIListener
    {
        void NodoClicked(NodoUI nodo, Point coord);
    }
}
