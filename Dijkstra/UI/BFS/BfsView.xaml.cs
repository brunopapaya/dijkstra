﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AlgorithmsVisualizer.UI;
using System.Windows.Media.Animation;
using AlgorithmsVisualizer.Algorithms;

namespace AlgorithmsVisualizer.UI
{
    /// <summary>
    /// Interaction logic for BfsUI.xaml
    /// </summary>
    public partial class BfsView : UserControl,IGrafoListener
    {

        GrafoUI mGrafoUI;
        NodoUI origen, destino;
        int paso = 1;

        public BfsView()
        {
            InitializeComponent();
            mGrafoUI = new GrafoUI(GraphCanvas);
        }

        private void ContinueMouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (paso)
            {
                case 1:
                    if (mGrafoUI.GetSize() < 6)
                    {
                        MessageBox.Show("¡El grafo debe tener por lo menos 6 nodos!", "Advertencia");
                    }
                    else
                    {
                        paso = 2;
                        mGrafoUI.Mode = GrafoModo.SELECCION;
                        mGrafoUI.GrafoUIListener = this;
                        Hint.Text = "2. Selecciona el nodo de origen";
                        Storyboard sb = this.FindResource("HintFade2") as Storyboard;
                        sb.Begin();

                    }
                    break;
                case 2:
                    if (origen == null)
                        MessageBox.Show("Escoge el nodo de origen");
                    else
                    {
                        paso = 3;
                        origen.Locked = true;
                        Hint.Text = "3. Selecciona el nodo destino";
                        Storyboard sb = this.FindResource("HintFade2") as Storyboard;
                        sb.Begin();
                    }
                    break;
                case 3:
                    if (destino == null)
                        MessageBox.Show("Escoge el nodo destino", "Advertencia");
                    else
                    {
                        ComenzarBFS();
                    }
                    break;
            }

        }

        private async void ComenzarBFS()
        {
            //Iniciar bfs

            BfsUI DfsUI = new BfsUI(mGrafoUI,CallStack);
            await DfsUI.Resolver(origen, destino);
        }

        public void NodoWasSelected(NodoUI nodo)
        {
            switch (paso)
            {
                case 2:
                    if (origen != null)
                        origen.Selected = false;
                    origen = nodo;
                    origen.Selected = true;
                    break;
                case 3:
                    if (nodo.Locked)
                    {
                        MessageBox.Show("El nodo de origen no puede ser igual al nodo destino", "Advertencia");
                    }
                    else
                    {
                        if (destino != null)
                            destino.Selected = false;
                        destino = nodo;
                        destino.Selected = true;

                    }
                    break;
            }
        }
    }
}
