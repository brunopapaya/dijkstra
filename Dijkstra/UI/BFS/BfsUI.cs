﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorithmsVisualizer.Algorithms;
using AlgorithmsVisualizer.Structures;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;

namespace AlgorithmsVisualizer.UI
{
    public class BfsUI : BFS<NodoUI>
    {
        public int DELAY { get; set; }

        private StackPanel CallStack;

        public BfsUI(GrafoUI pGrafo,StackPanel callStack) : base(pGrafo)
        {
            CallStack = callStack;
            DELAY = 1500;
        }   

        public override async Task EvaluandoArista(Nodo origen, Arista arista)
        {
            NodoUI nodoOrigen = origen as NodoUI;
            AristaUI aristaUI = (from a in nodoOrigen.AristasVisuales where a.IndiceDestino == arista.IndiceDestino select a).FirstOrDefault();
            if (aristaUI != null)
            {
                aristaUI.Highlight(true);
                await Task.Delay(DELAY);
                aristaUI.Highlight(false);
            }
            else
            {
                Console.WriteLine("Arista es nula!! Revisar porque");
            }
        }
        public override async Task NodoEnEstado(Nodo nodo,Estados estado)
        {
            NodoUI nodoUI = nodo as NodoUI;
            nodoUI.SetState(estado);
        }
        public override async Task Encolando(List<Nodo> ruta)
        {
            NodoUI nodoUI = ruta.Last() as NodoUI;
            nodoUI.SetState(Estados.POR_EVALUAR);

            TextBlock tb = new TextBlock();
            tb.VerticalAlignment = VerticalAlignment.Center;
            tb.HorizontalAlignment = HorizontalAlignment.Center;
            tb.TextAlignment = TextAlignment.Center;
            string temp = "";
            int c = 0;
            foreach (Nodo nodo in ruta)
            {
                if (c == 0)
                {
                    temp = nodo.ToString();
                    c++;
                }
                else
                    temp = temp + " -> " + nodo.ToString();
            }
            tb.Text = temp;
            tb.IsEnabled = false;
            tb.Foreground = Brushes.LightGray;
            tb.FontSize = 25;
            CallStack.Children.Add(tb);
        }
        
        public override async Task Desencolando()
        {
            CallStack.Children.RemoveAt(0);
        }
        public override async Task EncontreObjetivo(List<Nodo> nodo)
        {

        }

    }
}
