﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorithmsVisualizer.Algorithms;
using AlgorithmsVisualizer.Structures;
using System.Windows.Media;

namespace AlgorithmsVisualizer.UI
{
    public class DfsUI : DFS<NodoUI>
    {

        public DfsUI(GrafoUI pGrafo) : base(pGrafo)
        {

        }

        public override async Task EvaluandoArista(Nodo origen, Arista arista)
        {
            NodoUI nodoOrigen = origen as NodoUI;
            AristaUI aristaUI = (from a in nodoOrigen.AristasVisuales where a.IndiceDestino == arista.IndiceDestino select a).FirstOrDefault();
            if (aristaUI != null)
            {
                aristaUI.Highlight(true);
                await Task.Delay(500);
                aristaUI.Highlight(false);
            }
            else
            {
                Console.WriteLine("Arista es nula!! Revisar porque");
            }
        }
        public override async Task MarcarComoVisitado(Nodo nodo)
        {
            NodoUI nodoUI = nodo as NodoUI;
            nodoUI.SetColor(Brushes.DarkGray);
        }
        public override async Task AgregandoStack(int nodo)
        {

        }
        public override async Task SacandoStack(int nodo)
        {

        }
        public override async Task EncontreObjetivo(Nodo nodo)
        {

        }

    }
}
