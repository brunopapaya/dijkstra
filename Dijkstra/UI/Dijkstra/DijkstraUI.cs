﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlgorithmsVisualizer.Structures;
using AlgorithmsVisualizer.Algorithms;
using System.Threading;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;

namespace AlgorithmsVisualizer.UI
{
    public class DijkstraUI : Dijkstra<NodoUI>
    {
        private Grid MonitorTable;
        private List<TextBlock> listDistancias,listComoLlego;

        public int DELAY { get; set; }

        public DijkstraUI(GrafoUI pGrafo) : base(pGrafo)
        {
            DELAY = 300;
        }

        public void InicializarTabla(Grid pGrid)
        {
            MonitorTable = pGrid;

            if (MonitorTable != null)
            {
                MonitorTable.RowDefinitions.Clear();
                MonitorTable.ColumnDefinitions.Clear();
                MonitorTable.Children.Clear();
                int nodos = mGrafo.GetSize();
                listDistancias = new List<TextBlock>(nodos);
                listComoLlego = new List<TextBlock> (nodos);
                //Setear las columnas
                string[] columnas = new string[] { "Nodo","Distancia","¿Cómo llego?" };
                int c = 0;
                RowDefinition header = new RowDefinition();
                header.Height = new GridLength(30);
                MonitorTable.RowDefinitions.Add(header);
                foreach (string col in columnas)
                {
                    ColumnDefinition cd = new ColumnDefinition();
                    cd.Width = new GridLength(1, GridUnitType.Star);
                    MonitorTable.ColumnDefinitions.Add(cd);

                    TextBlock tb = new TextBlock();
                    tb.VerticalAlignment = VerticalAlignment.Center;
                    tb.HorizontalAlignment = HorizontalAlignment.Center;
                    tb.Text = col;
                    tb.Foreground = Brushes.DarkGreen;
                    tb.FontWeight = FontWeights.Bold;
                    Grid.SetColumn(tb, c++);
                    MonitorTable.Children.Add(tb);
                }

                //Llenar tabla
                RowDefinition tempRow;
                int r = 1;
                double width = MonitorTable.ActualWidth / columnas.Count();
                foreach (Nodo nodo in mGrafo.Nodos)
                {
                    tempRow = new RowDefinition();
                    tempRow.Height = new GridLength(30);
                    MonitorTable.RowDefinitions.Add(tempRow);

                    Brush background = new SolidColorBrush(Color.FromArgb((byte)(r%2==0?80:180), (byte)100, (byte)194, (byte)74)); ;

                    TextBlock tbNodo = GetNormalText(nodo.ToString(),background);
                    tbNodo.Width = width;   tbNodo.Height = 30;
                    Grid.SetColumn(tbNodo,0);
                    Grid.SetRow(tbNodo,r);

                    TextBlock tbDistancia = GetNormalText("INF", background);
                    tbDistancia.Width = width; tbDistancia.Height = 30;
                    Grid.SetColumn(tbDistancia, 1);
                    Grid.SetRow(tbDistancia, r);

                    TextBlock tbComoLlego = GetNormalText("-1", background);
                    tbComoLlego.Width = width; tbComoLlego.Height = 30;
                    Grid.SetColumn(tbComoLlego, 2);
                    Grid.SetRow(tbComoLlego, r++);

                    MonitorTable.Children.Add(tbNodo);
                    MonitorTable.Children.Add(tbDistancia);
                    MonitorTable.Children.Add(tbComoLlego);

                    listDistancias.Add(tbDistancia);
                    listComoLlego.Add(tbComoLlego);

                }


            }
            
        }

        private TextBlock GetNormalText(string texto,Brush backgroundColor)
        {
            TextBlock tb = new TextBlock();
            tb.VerticalAlignment = VerticalAlignment.Center;
            tb.HorizontalAlignment = HorizontalAlignment.Center;
            tb.TextAlignment = TextAlignment.Center;
            tb.Text = texto;
            //tb.Background = backgroundColor;
            tb.IsEnabled = false;
            tb.Foreground = Brushes.LightGray;
            return tb;
        }

        public void Limpiar()
        {
            mGrafo.Limpiar();
        }

        public override async Task EvaluandoArista(Nodo origen,Arista arista)
        {
            NodoUI nodoOrigen = origen as NodoUI;
            AristaUI aristaUI = (from a in nodoOrigen.AristasVisuales where a.IndiceDestino == arista.IndiceDestino select a).FirstOrDefault();
            if (aristaUI != null)
            {
                aristaUI.Highlight(true);
                await Task.Delay(DELAY);
                aristaUI.Highlight(false);
            }
            else
            {
                Console.WriteLine("Arista es nula!! Revisar porque");
            }
        }

        public override async Task NodoEnEstado(Nodo nodo, Estados estado)
        {
            NodoUI nodoUI = nodo as NodoUI;
            nodoUI.SetState(estado);
        }

        public override async Task AgregandoARuta(Nodo nodo)
        {
            NodoUI nodoUI = nodo as NodoUI;
            nodoUI.SetState(Estados.SOLUCION);
            await Task.Delay(DELAY/2);
        }

        public override async Task SeteandoComoLlego(Nodo destino, Nodo comoLLego)
        {
            NodoUI nodoUI = (NodoUI)destino;
            TextBlock view = listComoLlego[destino.Indice];
            Brush color = view.Background;
            view.Background = Brushes.LightGreen;
            view.Foreground = Brushes.White;
            view.Text = comoLLego.ToString();
            nodoUI.Selected = true;
            await Task.Delay(DELAY);
            view.Background = color;
            nodoUI.Selected = false;
            view.Foreground = Brushes.DarkGray; 
        }

        public override async Task SeteandoDistancia(int nodo, int peso)
        {
            TextBlock view = listDistancias[nodo];
            Brush color = view.Background;
            view.Background = Brushes.LightGreen;
            view.Foreground = Brushes.White;
            view.Text = "" + peso;
            await Task.Delay(DELAY);
            view.Background = color;
            view.Foreground = Brushes.DarkGray;
        }


    }
}
